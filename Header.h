#pragma once
#include <iostream>
#include <list>
#include <vector>
using namespace std;
#ifndef Header_H
#define Header_H
class Node;
class Edge
{
public:
	Edge(Node* start, Node* end, int weight)
	{
		Start = start;
		End = end;
		Weight = weight;
	}
	Node* get_start() { return Start; }
	Node* get_end() { return End; }
	int get_weight() { return Weight; }
private:
	int Weight;
	Node* Start;
	Node* End;
	
};
class Node
{
public:
	void addEdge(Node* end, int weight) //add edge between node
	{
		Edge listedge(this, end, weight);
		edge.push_back(listedge); //push edge to list 
	}
	void printEdge() // show Edge
	{
		cout << "Node: " << get_name() << endl;
		if (edge.empty())
		{
			cout << " No Destination" << endl;
		}
		for (auto it = edge.begin(); it != edge.end(); it++)
		{
			Edge  pair = *it;
			cout << " --> " << pair.get_end()->get_name() << " with weight " << pair.get_weight() << endl;
		}
		cout << "**********************************************" << endl;
	}
	void set_name(char new_name) { name = new_name; } //set the name to new name
	char get_name() { return name; }
	list<Edge> get_edge() { return edge; }
private:
	char name;
	list<Edge> edge;
};
class Graph
{
public:
	void printGraph() //print graph
	{
		for (auto it = nodes.begin(); it != nodes.end(); it++)
		{
			Node nodes = *it;
			nodes.printEdge(); //print edge of each node
		}
	}
	void arrayToList(int** arr, int node_size) //change adjacency matrix[array] to list
	{
		cout << "**********************************************" << endl;
		Node node_name;
		for (int i = 0; i < node_size; i++)
		{
			node_name.set_name(65 + i); // set the name
			nodes.push_back(node_name); //push the name of node
		}
		int row = 0;
		for (auto it = nodes.begin(); it != nodes.end(); it++)
		{
			for (int column = 0; column < node_size; column++)
			{
				Node* end = new Node;
				Node& start = *it;
				if (arr[row][column] == 0)
				{
					continue;
				}
				else {
					end->set_name(65 + column);
					start.addEdge(end, arr[row][column]);
				}
			}
			row++; //change row
		}
	}

	bool pseudoGraph()
	{
		for (auto it = nodes.begin(); it != nodes.end(); it++) //traverse to every node 
		{
			Node nodes = *it;
			list<Edge> node_edge = nodes.get_edge(); //get edge between each nodes
			for (auto it2 = node_edge.begin(); it2 != node_edge.end(); it2++)
			{
				Node* start = it2->get_start();
				Node* end = it2->get_end();
				if (start->get_name() == end->get_name() && it2->get_weight() != 0)
				{
					return true;
				}
			}
		}
		return false;
	}

	bool multiGraph() //adjacency matrix cannot be a multigraph
	{
		return false;
	}

	bool digraph()
	{
		for (auto it = nodes.begin(); it != nodes.end(); it++) //traverse to every node
		{
			Node node = *it;
			list<Edge> node_edge = node.get_edge(); //get edge between each nodes
			for (auto it2 = node_edge.begin(); it2 != node_edge.end(); it2++)
			{
				Node* start = it2->get_start();
				Node* end = it2->get_end();
				for (auto it3 = nodes.begin(); it3 != nodes.end(); it3++)
				{
					Node node_2 = *it3;
					list<Edge> node_edge2 = node_2.get_edge(); //get edge between each nodes
					if (node_2.get_name() != node.get_name())
					{
						for (auto it4 = node_edge2.begin(); it4 != node_edge2.end(); it4++)
						{
							Node* start_2 = it4->get_start();
							Node* end_2 = it4->get_end();
							if (start->get_name() == end_2->get_name() && start_2->get_name() == end->get_name() && (start_2->get_name() != end->get_name() || start_2->get_name() != end_2->get_name()))
							{
								return false;
							}
						}
					}
				}
			}
		}
	}
	bool weightGraph()
	{
		for (auto it = nodes.begin(); it != nodes.end(); it++) //traverse to every node
		{
			Node nodes = *it;
			list<Edge> node_edge = nodes.get_edge(); //get edge between each nodes
			for (auto it2 = node_edge.begin(); it2 != node_edge.end(); it2++)
			{
				if (it2->get_weight() != 0)
				{
					return true;
				}
			}
		}
		return false;
	}
	bool completeGraph()
	{
		int count = 0;
		for (auto it = nodes.begin(); it != nodes.end(); it++) //traverse to every node
		{
			Node nodes = *it;
			list<Edge> node_edge = nodes.get_edge(); //get edge between each nodes
			for (auto it2 = node_edge.begin(); it2 != node_edge.end(); it2++)
			{
				Node* start = it2->get_start();
				Node* end = it2->get_end();
				count++;
				if (start->get_name() == end->get_name()) //remove pseudograph
				{
					count--;
				}
			}
		}
		if (count == nodes.size() * (nodes.size() - 1) / 2)
		{
			return true;
		}
		return false;
	}
	void dykstra(char start) //find shortest path
	{
		char name_start = start;
		vector<int> short_weight;
		vector<char> short_destination;
		vector<int> weight;
		vector<char> destination;
		short_weight.push_back(0); //the shortest weight is 0
		short_destination.push_back(start);
		weight.push_back(0);
		destination.push_back(start);
		for (auto it = nodes.begin(); it != nodes.end(); it++) //traverse to every node
		{
			Node node = *it;
			list<Edge> node_edge = node.get_edge(); //get edge between each nodes
			for (auto it2 = node_edge.begin(); it2 != node_edge.end(); it2++)
			{
				Node* strt = it2->get_start();
				Node* end = it2->get_end();
				int distance = it2->get_weight();
				if (strt->get_name() == start)
				{
					weight.push_back(distance);
					destination.push_back(end->get_name());
				}
				int find_min = INT_MAX;
				char min_name = ' ';
				for (int i = 0; i < weight.size(); i++) //intialize
				{
					if (weight[i] > 0)
					{
						min_name = destination[i];
						find_min = weight[i];
						break;
					}
				}
				for (int i = 0; i < weight.size(); i++) //update
				{
					if (find_min > weight[i] && weight[i] > 0)
					{
						find_min = weight[i];
						min_name = destination[i];
					}
				}
				short_destination.push_back(min_name);
				short_weight.push_back(find_min);
				start = min_name;
				for (int i = 0; i < destination.size(); i++)
				{
					if (destination[i] == min_name)
					{
						weight[i] = 0;
					}
				}
				while (true)
				{
					for (auto it3 = nodes.begin(); it3 != nodes.end(); it3++)
					{
						Node node_2 = *it3;
						list<Edge> node_edge2 = node_2.get_edge(); //get edge in each nodes
						for (auto it4 = node_edge.begin(); it4 != node_edge.end(); it4++)
						{
							Node* strt = it4->get_start();
							Node* end = it4->get_end();
							int distance = it4->get_weight();
							bool status = false;
							if (strt->get_name() == start)
							{
								for (int i = 0; i < short_destination.size(); i++)
								{
									if (short_destination[i] == end->get_name()) 
									{
										status = true;
										break;
									}
								}
								if (status == false)
								{
									for (int i = 0; i < destination.size(); i++)
									{
										if (destination[i] == end->get_name())
										{
											if (weight[i] > find_min + distance)
											{
												weight[i] = find_min + distance;
												status = true;
												break;
											}
										}
									}
									if (status == false)
									{
										weight.push_back(distance + find_min); //shortest value from start to destination
										destination.push_back(end->get_name());
									}
								}
							}
						}
					}
					for (int i = 0; i < weight.size(); i++)
					{
						if (weight[i] > 0)
						{
							min_name = destination[i];
							find_min = weight[i];
							break;
						}
					}
					for (int i = 0; i < weight.size(); i++)
					{
						if (find_min > weight[i] && weight[i] > 0)
						{
							min_name = destination[i];
							find_min = weight[i];
						}
					}
					if (min_name != short_destination.back())
					{
						short_destination.push_back(min_name);
						short_weight.push_back(find_min);
					}
					for (int i = 0; i < destination.size(); i++)
					{
						if (destination[i] == min_name)
						{
							weight[i] = 0;
						}
					}
					int count = 0;
					start = min_name;
					for (int i = 0; i < weight.size(); i++)
					{
						if (weight[i] == 0)
						{
							count++;
						}
					}
					if (count == weight.size())
					{
						break;
					}
				}
				//print shortest distance
				int i = 0;
				int loop = 0;
				while (i != short_destination.size())
				{
					if (name_start == short_destination[i] && short_weight[i] > 0)
					{
						cout << "";
					}
					else if (short_weight[i] == INT_MAX)
					{
						cout << "";
					}
					else
						cout << name_start << " To " << short_destination[i] << " with distance " << short_weight[i] << endl;
					i++;
					loop++;
				}
				if (short_destination.size() - 1 != nodes.size())
				{
					cout << "Remain destination is Unreachable";
				}
			}
		}
	}
private:
	list<Node> nodes;
};
#endif#pragma once
